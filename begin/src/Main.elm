module Main exposing (..)
import Html exposing (..)
import Browser exposing (sandbox)
import Html.Attributes exposing (placeholder)
import Html.Events exposing (..)

-- model

type alias Model =
        {message : String}

init: Model
init =
        {message = "Initial State"}


-- our update


type Msg
        = Change String

update : Msg -> Model -> Model 
update msg model =
        case msg of 
                Change newMessage ->
                        {model | message = newMessage}



-- our view

view : Model -> Html Msg
view model =
        div []
        [
                input [placeholder "elm work flow", onInput Change] []
                , div [] [text model.message]
                ]



-- main

main =
        sandbox 
        { init = init
        , view = view
        , update = update
        }
