module Photogroove exposing (main)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Browser
import Array exposing(Array)

--- Model
type alias Photo =
  {url:String}


type alias Model =
  {photos: List Photo , selectedUrl:String, chosenSize: ThumbnailSize}



initialModel : Model
initialModel =
  { photos =
     [ {url = "99_hands.jpg"}
     , {url = "Ryse.jpg"}
     , {url = "city_hero.jpg"}
     ]
   , selectedUrl = "99_hands.jpg"
   , chosenSize = Medium
   }

photoArray : Array Photo
photoArray =
  Array.fromList initialModel.photos


urlPrefix : String
urlPrefix =
  "../static/"

--- view
type ThumbnailSize 
  = Small
  | Medium
  | Large

{-
chosenSize : ThumbnailSize
chosenSize = Small
chosenSize : ThumbnailSize
chosenSize = Medium
chosenSize : ThumbnailSize
chosenSize = Large
-}

view : Model -> Html Msg
view model =
     div [class "content"]
         [ h1 [] [text "Mon PhotoGroove avec ELM"]
           , button [ 
                onClick ClickedSurpriseMe
             ][text "Surpise Me!"]
           , div [id "thumbnails"] {-(
             List.map (\photo -> viewThumbnail model.selectedUrl photo)
             model.photos) -}
           (List.map (viewThumbnail model.selectedUrl) model.photos)
           , img [ class "large"
           , src (urlPrefix ++ {-"large/" ++-} model.selectedUrl)
           ] []
           , h3[][text "Thumbnail Size"]
           , div [id"choose-size"]{-[viewSizeChooser Small, viewSizeChooser Medium, viewSizeChooser Large]-}
                                  (List.map viewSizeChooser [Small,Medium,Large])
           , div [id "thumbnails", class (sizeToString model.chosenSize)](List.map (viewThumbnail model.selectedUrl)model.photos)
          ]
{- 
viewThumbnail selectedUrl thumb =
  if selectedUrl == thumb.url then
    img[src (urlPrefix ++ thumb.url), class "selected"][]
  else
    img [src (urlPrefix ++ thumb.url)][]
-}
viewThumbnail : String -> Photo -> Html Msg
viewThumbnail selectedUrl thumb =
  img [src (urlPrefix ++ thumb.url)
  , classList[("selected", selectedUrl == thumb.url)]
  , onClick (ClickedPhoto thumb.url)
  ] []

viewSizeChooser : ThumbnailSize -> Html Msg
viewSizeChooser size =
  label []
  [
    input [type_ "radio", name "size", onClick (ClickedSize size)] []
  , text (sizeToString size)
  ]

sizeToString : ThumbnailSize -> String
sizeToString size = 
  case size of
    Small ->
      "small"
    Medium ->
      "med"
    Large ->
      "large"

-- update

getPhotoUrl : Int -> String
getPhotoUrl index =
  case Array.get index photoArray of
    Just photo -> photo.url
    Nothing -> ""

type Msg = ClickedPhoto String
  | ClickedSize ThumbnailSize
  | ClickedSurpriseMe
update : Msg -> Model -> Model
update msg model =
  case msg of
    ClickedPhoto url ->
       {model | selectedUrl = url}

    ClickedSize size ->
      {model| chosenSize = size}

   -- ClickedSurpriseMe index ->
   -- {model | selectedUrl = "Ryse.jpg"}

    ClickedSurpriseMe ->
       {model | selectedUrl = "Ryse.jpg"}
{-
  if msg.description == "ClickedPhoto" then
    {model | selectedUrl = msg.data}
  else if msg.description == "ClickedSurpriseMe" then
    {model | selectedUrl = "Ryse.jpg"}
  else  
    model
-}
--- main
main =
  Browser.sandbox
  { init = initialModel
  , view = view
  , update = update
  }
