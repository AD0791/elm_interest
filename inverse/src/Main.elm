module Main exposing (..)
import Browser 
import Html exposing (..)
import Html.Events exposing (onInput)
import Html.Attributes exposing (..)



-- main

main = 
        Browser.sandbox { init = init, update = update, view = view }


-- model

type alias Model =
        {
                content: String
        }

init : Model
init = 
        {
                content = "Elm can be beautiful"
        }


-- update

type Msg =
        Change String


update : Msg -> Model -> Model
update msg model =
        case msg of
                Change newContent ->
                        {
                                model | content = newContent
                        }


-- view

view : Model -> Html Msg
view model =
        div [] [
                  input [placeholder "Text to reverse", value model.content, onInput Change] []
                , div [][text (String.reverse model.content)]
                ]
